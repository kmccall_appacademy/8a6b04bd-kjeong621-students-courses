class Student

  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    @first_name + " " + @last_name
  end

  def enroll(course_obj)
    if !@courses.include?(course_obj)
      @courses.each do |course|
        if course.conflicts_with?(course_obj)
          raise "Already enrolled course"
        end
      end
      @courses << course_obj
      course_obj.students << self
    end
  end

  def course_load
    total_load = {}
    @courses.each do |course|
      if total_load.keys.include?(course.department)
        total_load[course.department] += course.credits
      else
        total_load[course.department] = course.credits
      end
    end
    total_load
  end
end
